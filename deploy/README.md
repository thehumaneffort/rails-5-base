# Deployment README

This project is a rails project with a REDIS cache and a PostgreSQL database. It
is set up to be seamlessly deployed to a google cloud situation, but can easily
be configured for any Kubernetes cluster.  The deploy directory contains all of
the dependencies required, and the template file for the different kube deploys.
Staging, production, and branch deploys all use the same postgres and redis
instances,  with namespaced databases.  It is currently not set up for automatic
backup or anything clever like that.

To prepare a sparkly new environment in Google Cloud for the application:

```
cd deploy
bash prepare.sh
```

Now answer the questions (or use defaults) and let the magic happen.
