#!/bin/bash

set -e

DEFAULT_NAME=$(cd .. && basename $PWD && cd deploy)
read -e -p "Project Name (default: $DEFAULT_NAME): " NAME_IN
NAME=${NAME_IN:-$DEFAULT_NAME}

read -p 'Postgres disk size (in GB, default 10): ' SIZE_IN
POSTGRES_DISK_SIZE=${SIZE_IN:-10}

read -p 'Google cloud disk zone (default: us-central1-c): ' ZONE_IN
ZONE=${ZONE_IN:-us-central1-c}

read -p 'Docker registry host (default: registry.gitlab.com): ' REGISTRY_IN
DOCKER_REGISTRY_SERVER=${REGISTRY_IN:registry.gitlab.com}

read -p 'Docker registry email: ' DOCKER_EMAIL
read -p 'Docker registry username: ' DOCKER_USERNAME
read -p 'Docker registry password: ' DOCKER_PASSWORD

sed -e 's|__NAMESPACE__|'$NAME'|g' -f kube.tmpl.yaml > kube.tmpl.yaml.out
mv kube.tmpl.yaml.out > kube.tmpl.yaml

kubectl config use-context the-core

echo Creating namespace
kubectl get namespace $NAME -o name || kubectl create namespace $NAME

echo Creating PG auth secret
kubectl get secret pg-auth --namespace=$NAME -o name || \
		kubectl create secret generic pg-auth --namespace=$NAME \
		--from-literal=username=$NAME-pg \
		--from-literal=password=p$(openssl rand -hex 32)

echo Creating rails secret key base secret
kubectl get secret secret-key --namespace=$NAME -o name || \
		kubectl create secret generic secret-key --namespace=$NAME \
		--from-literal=key-base=p$(openssl rand -hex 64)



echo Creating PG disk
if [ $$(gcloud compute disks list $NAME-pg-disk | grep "$NAME-pg-disk" | wc -l) == 0 ] ; then \
		gcloud compute disks create $NAME"-pg-disk" --size $POSTGRES_DISK_SIZE --type pd-ssd --zone $ZONE ; fi

echo Deploying kubernetes dependencies
cat ./kube-deps.yaml | sed 's|__NAMESPACE__|'$NAME'|' | kubectl apply --namespace=$NAME -f -

kubectl create secret docker-registry registry --docker-server=$DOCKER_REGISTRY_SERVER \
  --docker-username=$DOCKER_USERNAME \
  --docker-password=$DOCKER_PASSWORD \
  --docker-email=$DOCKER_EMAIL

echo "Done, your dependencies are set."
