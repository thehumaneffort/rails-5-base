NAME:=rails-base
VERSION:=v$(shell if [ "$$CI_BUILD_REF" = "" ] ; then date +%YM%mD%dH%HM%M ;  else echo $$CI_BUILD_REF; fi )
DOCKER_BASE=registry.gitlab.com/thehumaneffort
IMG=$(DOCKER_BASE)/$(NAME):$(VERSION)
KUBE_TMPL=deploy/kube.tmpl.yaml
STAGING_DB=$(NAME)_staging
BRANCH_DB=$(NAME)_$(CI_ENVIRONMENT_SLUG)

ci-login: FORCE
	docker login -u gitlab-ci-token -p $(CI_BUILD_TOKEN) registry.gitlab.com

ci-base-image: ci-login  FORCE
	docker build -t $(DOCKER_BASE)/$(NAME):base -f Dockerfile-dev-base .
	docker push $(DOCKER_BASE)/$(NAME):base
	docker rmi $(DOCKER_BASE)/$(NAME):base

	docker build -t $(DOCKER_BASE)/$(NAME):prod-base -f Dockerfile-prod-base .
	docker push $(DOCKER_BASE)/$(NAME):prod-base
	docker rmi $(DOCKER_BASE)/$(NAME):prod-base

ci-build: ci-login FORCE
	docker build -t $(IMG) -f Dockerfile .

ci-push: ci-login FORCE
	docker push $(IMG)

ci-clean: FORCE
	docker rmi $(IMG)

launch-staging:
	cat $(KUBE_TMPL) | \
		sed 's|__NAME__|$(NAME)-staging|' | \
		sed 's|__NAMESPACE__|$(NAME)|' | \
		sed 's|__IMAGE__|$(IMG)|' | \
		sed 's|__ENV__|staging|' | \
		sed 's|__SLUG__|staging|' | \
		sed 's|__WORKERS__|1|' | \
		sed 's|__VIRTUAL_HOST__|$(HOSTNAME)/*|' | \
		kubectl apply -f - --namespace=$(NAME)

launch-branch:
	kubectl exec --namespace=$(NAME) $(shell kubectl get pod --namespace=$(NAME) | grep postgres | awk '{print $$1}') -- bash -c 'export PGHOST=postgres PGUSER=$$POSTGRES_USER PGPASSWORD=$$POSTGRES_PASSWORD; pg_dump -Fc --no-acl --no-owner -d $(STAGING_DB) > /tmp/dbdump.pgd; dropdb $(BRANCH_DB); createdb $(BRANCH_DB); pg_restore -Fc --no-acl --no-owner -d $(BRANCH_DB) < /tmp/dbdump.pgd ; rm /tmp/dbdump.pgd'

	cat $(KUBE_TMPL) | \
		sed 's|__NAME__|$(CI_ENVIRONMENT_SLUG)|' | \
		sed 's|__NAMESPACE__|$(NAME)|' | \
		sed 's|__IMAGE__|$(IMG)|' | \
		sed 's|__ENV__|staging|' | \
		sed 's|__SLUG__|$(CI_ENVIRONMENT_SLUG)|' | \
		sed 's|__WORKERS__|1|' | \
		sed 's|__VIRTUAL_HOST__|$(HOSTNAME)/*|' | \
		kubectl apply -f -  --namespace=$(NAME)

stop-branch:
	cat $(KUBE_TMPL) | \
		sed 's|__NAME__|$(CI_ENVIRONMENT_SLUG)|' | \
		sed 's|__NAMESPACE__|$(NAME)|' | \
		sed 's|__IMAGE__|$(IMG)|' | \
		sed 's|__ENV__|staging|' | \
		sed 's|__SLUG__|staging|' | \
		sed 's|__WORKERS__|1|' | \
		sed 's|__VIRTUAL_HOST__|$(HOSTNAME)/*|' | \
		kubectl delete -f -  --namespace=$(NAME)
	kubectl exec --namespace=$(NAME) $(shell kubectl get pod --namespace=$(NAME) | grep postgres | awk '{print $$1}') -- bash -c 'export PGHOST=postgres PGUSER=$$POSTGRES_USER PGPASSWORD=$$POSTGRES_PASSWORD; dropdb $(BRANCH_DB)'


FORCE:
