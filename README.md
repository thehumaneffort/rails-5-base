# RailsBase

To start a project based on this starting point, create a new git repo:

## Starting a project based on RailsBase:
```
git clone git@gitlab.com:thehumaneffort/rails-5-base.git new-awesome-project
```

Rename the remote:

```
git remote rename origin upstream
```

Now change every instance of rails-base railsbase RailsBase rails_base and
'rails base' to the name of your application, delete this getting started note,
and move the following sections to the end of your readme.

## Understanding rails-5-base

There are a few things that are different about rails-5-base from a normal rails
install:

1. It uses a docker-compose system for development
2. It uses Kubernetes for deployment (see the makefile and deploy dirs)
3. It uses the GitLab CI system for automatic building.
4. It uses a special, "orphaned" git branch ('base') to define a starting point.
   This  makes docker builds much faster and more efficient.  See that branch
   for details about how to move new dependencies into your 'base'.
5. It has customized bin/* files that ideally handle common use cases.

Known issues:

1. If you want to use binding.pry, you can't use the normal server setup, which
   is based on a headless Guard instance.  Replace the normal server command in 
   docker-compose with `tail -f /dev/null`, restart, and run rails using the 
   normal `bin/rails s` command, which should allow binding.pry to work as normal.



## This project is based on thehumaneffort/rails-5-base

The original copy of this repository is based at
https://gitlab.com/thehumaneffort/rails-5-base. It's mainly for our convenience
in starting new projects, but feel free to use it yourself.  If you have changes
you think should be in every rails 5 project going forward, push them back into
the original repo if you have the privileges for that, or do it to your own fork
and start a pull request.

### Pulling in upstream changes into the current branch:

```
git fetch upstream
git merge upstream/master
```

### Pushing changes to upstream

Do this carefully!  Do not just copy/paste and execute.
```
git fetch upstream
# checkout upstream/master into detached head:
git checkout upstream/master
# checkout a branch you can deal with:
git checkout -b upstream-update
git cherry-pick <your changes to backport>
# fix inevitable merge conflicts

# now push our changes to the upstream master:
git push upstream upstream-update:master

# get back to work:
git checkout master

# This force-deletes upstream-master, only do this if
# you are sure you've pushed things properly:
git branch -D upstream-master
```

## Getting started

Make sure you have dinghy, docker, docker-machine, and docker-compose installed.

```
brew install docker dinghy docker-machine docker-compose
dinghy create
dinghy up
```

Now start everything:

```
docker-compose up -d && bin/setup
```

You should now have a version running at http://rails.railsbase.docker/

Yay!
